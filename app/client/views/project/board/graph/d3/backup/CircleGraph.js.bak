"use strict";

import prune from "underscore.string/prune";
import d3 from "d3";
import D3Graph from './D3Graph';

const traverse = [{
    linkType: "sourceLinks",
    nodeType: "target"
}, {
    linkType: "targetLinks",
    nodeType: "source"
}];

export default class CircleGraph extends D3Graph {

    constructor( el, props ) {
        super( el, props );

        this.name = "circleGraph";

        this.radiusScale = d3.scale.log().domain( [1, 100] ).range( [8, 128] ).clamp( true );

        this.nodes = [];
        this.links = [];
    }

    initSVG() {
        super.initSVG();

        // Create the tooltip element
        this.tooltip = this.graph.append( 'div' )
            .attr( 'class', 'tooltip' );

        // Arrow marker
        this.svg.append( "defs" )
            .selectAll( "marker" )
            .data( ["linked"] )
            .enter()
            .append( "marker" )
            .attr( "id", d => d )
            .attr( "viewBox", "0 -5 10 10" )
            .attr( "refX", 10 )
            .attr( "refY", 0 )
            .attr( "markerWidth", 10 )
            .attr( "markerHeight", 10 )
            .attr( "orient", "auto" )
            .append( "path" )
            .attr( "d", "M0,-5L10,0L0,5 L10,0 L0, -5" );

        // Links
        this.linksGroup = this.canvas.append( 'g' );

        // Nodes
        this.nodesGroup = this.canvas.append( 'g' );
    }

    initLayout() {
        super.initLayout();

        const { project } = this.props;

        const self    = this;
        const padding = 12;

        // Draw line to the circle's outside instead of center
        function lineX( source, target ) {
            const length = Math.sqrt( Math.pow( target.y - source.y, 2 ) + Math.pow( target.x - source.x, 2 ) );
            const scale  = (length - target.interpolatedRadius) / length;
            const offset = (target.x - source.x) - (target.x - source.x) * scale;
            return target.x - offset;
        }

        function lineY( source, target ) {
            const length = Math.sqrt( Math.pow( target.y - source.y, 2 ) + Math.pow( target.x - source.x, 2 ) );
            const scale  = (length - target.interpolatedRadius) / length;
            const offset = (target.y - source.y) - (target.y - source.y) * scale;
            return target.y - offset;
        }

        // Circle collision
        function collide( alpha ) {
            var quadtree = d3.geom.quadtree( self.nodes );
            return function ( d ) {
                var rb  = 2 * d.radius + padding,
                    nx1 = d.x - rb,
                    nx2 = d.x + rb,
                    ny1 = d.y - rb,
                    ny2 = d.y + rb;
                quadtree.visit( function ( quad, x1, y1, x2, y2 ) {
                    if ( quad.point && (quad.point !== d) ) {
                        var x = d.x - quad.point.x,
                            y = d.y - quad.point.y,
                            l = Math.sqrt( x * x + y * y );
                        if ( l < rb ) {
                            l = (l - rb) / l * alpha;
                            d.x -= x *= l;
                            d.y -= y *= l;
                            quad.point.x += x;
                            quad.point.y += y;
                        }
                    }
                    return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
                } );
            };
        }

        // Clustering (by project phase)
        function gravity( xalpha, yalpha ) {
            return function ( d ) {
                d.x += (d.cx - d.x) * xalpha;
                d.y += (self.height * 0.5 - d.y) * yalpha;
            };
        }

        // Interpolate radius
        function interpolateRadius( alpha ) {
            return function ( d ) {
                d.interpolatedRadius += (d.radius - d.interpolatedRadius) * alpha;
            };
        }

        this.force = d3.layout.force()
            .charge( node => node.radius * ( node.dummy ? 0 : -5 ) )
            .linkDistance( link => padding + Math.max(
                // Direct link relationship
                ( link.source.radius + link.target.radius ) * 2,
                // Actual node relationship
                ( Math.abs( link.source.phase - link.target.phase ) / project.phases.length ) * this.width
            ) )
            .linkStrength( link => this.linkStrength( ( Math.abs( link.source.phase - link.target.phase ) ) + 1 ) )
            .nodes( this.nodes )
            .links( this.links )
            .gravity( 0.03 )
            .on( 'tick', e => {
                this.node
                    .each( gravity( .1 * e.alpha, .05 * e.alpha ) )
                    .each( collide( 1.5 * e.alpha ) )
                    .each( interpolateRadius( .15 ) )
                    .attr( 'transform', n => 'translate(' + n.x + ',' + n.y + ')' );
                this.link
                    .attr( 'x1', l => lineX( l.source, l.target ) )
                    .attr( 'y1', l => lineY( l.source, l.target ) )
                    .attr( 'x2', l => lineX( l.target, l.source ) )
                    .attr( 'y2', l => lineY( l.target, l.source ) );
                /*this.link
                    .attr( "d", l =>
                        "M" + lineX( l.source, l.target ) + "," + lineY( l.source, l.target )
                        + "S" + ((l.source.x+ l.target.x)*0.5) + "," + ((l.source.y+ l.target.y)*0.5)
                        + " " + lineX( l.target, l.source ) + "," + lineY( l.target, l.source )
                    );*/
            } );
    }

    resize() {
        super.resize();

        // Update nodes gravity
        this.nodes.forEach( node => node.cx = this.phaseX( node.phase ) );
    }

    updateConstraints() {
        super.updateConstraints();

        const { project } = this.props;

        // Create a link strength scale from phases
        this.linkStrength = d3.scale.log().domain( [1, project.phases.length] ).range( [1, 0] ).clamp( true );

        // Update phase gravity
        this.phaseX = d3.scale.ordinal()
            .domain( d3.range( this.props.project.phases.length ) )
            .rangePoints( [0, this.width], 1 );
    }

    updateData() {
        super.updateData();

        const { project, cards } = this.props;

        if ( !cards ) {
            console.warn( 'Trying to update data of a CircleGraph without cards' );
            return;
        }

        // CARDS

        // Create a temporary list before replacing the previous one
        const oldNodes = this.nodes;

        // Update the nodes array, it needs to stay the same as this is the one referenced by d3
        this.nodes.splice( 0, this.nodes.length );

        cards.forEach( card => {
            // Get the previous node, we are going to reuse it if it exists because
            // it contains data that can be reused by d3 force layout
            const oldNode    = oldNodes.find( node => node.card._id == card._id );
            // Keep previous node or create a new one
            let node         = oldNode || {};
            node.card        = card;
            node.phase       = project.getPhase( card.phase ).index;
            node.radius      = this.getRadius( node );
            node.color       = this.getColor( node );
            node.sourceLinks = [];
            node.targetLinks = [];
            node.cx          = this.phaseX( node.phase );

            // If this is a fresh node, initialize its position
            if ( !oldNode ) {
                // Start without animating the radius change
                node.interpolatedRadius = node.radius;

                // Starting position, distribute a bit randomly (no collision) around where they should go
                node.px = node.x = this.phaseX( node.phase ) + ( ( ( Math.random() * 2 ) - 1 ) * ( this.width / project.phases.length ) );
                node.py = node.y = ( this.height * .25 ) + ( Math.random() * this.height / 2 );
            }

            this.nodes.push( node );
        } );

        // LINKS

        // Empty the links array, it needs to stay the same as this is the one referenced by d3
        this.links.splice( 0, this.links.length );

        // Compute links
        const cardLinks = _.reduce( this.nodes, ( links, node, index, nodes ) => {
            return links.concat(
                node.card.linkedCards
                    .map( linkedCardId => ({
                        source: nodes.find( n => n.card._id == linkedCardId ),
                        target: node
                    }) )
                    .filter( link => link.source ) // FIXME: Temporary solution for the bad graph, should remove references to links when deleting a card
            )
        }, [] );

        // Special nodes used to avoid node/link collision and draw curves
        cardLinks.forEach( link => {
            const source = link.source;
            const target = link.target;

            // Cache link information for highlighting paths in the graph
            source.sourceLinks.push( link );
            target.targetLinks.push( link );

            // Add links between source/target and dummies
            this.links.push(
                /*{
                 dummy:      true,
                 source:     source,
                 target:     bilink.dummy,
                 sourceCard: source,
                 targetCard: target
                 },*/
                {
                    source:     source,
                    target:     target,
                    sourceCard: source,
                    targetCard: target
                }/*,
                 {
                 dummy:      true,
                 source:     bilink.dummy,
                 target:     target,
                 sourceCard: source,
                 targetCard: target
                 }*/
            );
        } );
    }

    updateLayout() {
        super.updateLayout();

        // SVG Manipulation

        // NODES
        this.node = this.nodesGroup.selectAll( '.node' ).data( this.nodes );
        this.node.exit().remove();
        const card = this.node.enter().append( 'g' )
            .attr( 'class', 'node card' )
            .call( this.force.drag );

        // Card circle
        card.append( 'circle' )
            .attr( 'class', 'circle' )
            .on( 'mouseover', this.highlightConnectedNodes.bind( this ) )
            .on( 'mouseout', this.restoreConnectedNodes.bind( this ) );

        // Card label
        card.append( 'text' )
            .attr( 'class', 'name label' )
            .text( node => !node.dummy ? prune( node.card.name, 32 ) : '' );

        // Tooltip
        card.on( 'mousemove', node => {
                const mouse = d3.mouse( this.root.node() );
                this.tooltip.style( 'left', mouse[0] + 'px' ).style( 'top', (mouse[1] + 20) + 'px' );
            } )
            .on( 'mouseover', node => this.tooltip.classed( 'active', true ).html( node.card.name ) )
            .on( 'mouseout', node => this.tooltip.classed( 'active', false ) )
            .on( 'click', node => {
                if ( !d3.event.defaultPrevented ) {
                    this.props.openCard( node.card.slug || node.card._id );
                }
            } );

        // Update all
        this.node.select( '.circle' )
            .attr( 'fill', node => node.color )
            .attr( 'stroke', node => node.color )
            .attr( 'r', node => node.radius );

        this.node.select( '.label' )
            .attr( 'dx', node => Math.sqrt( ( node.radius * node.radius ) * 0.5 ) + 6 )
            .attr( 'dy', node => Math.sqrt( ( node.radius * node.radius) * 0.5 ) + 12 );

        // LINKS
        this.link = this.linksGroup.selectAll( '.link' ).data( this.links );
        this.link.exit().remove();
        this.link.enter().append( 'line' )
            .attr( 'class', 'link' )
            .attr( 'marker-end', 'url(#linked)' ); // Firefox doesn't like this being set in CSS
    }

    // Get node radius
    getRadius( node ) {
        let count = node.card[this.props.sizedBy];
        if ( isNaN( count ) ) {
            count = 0;
        }
        return this.radiusScale( count + 1 ); // +1 because of the log() method not going to 0
    }

    // Get node color
    getColor( node ) {
        return colorCollection[node.phase % colorCollection.length];
    }

    // Highlight node connections
    highlightConnectedNodes( node ) {
        let remainingNodes = [];
        let nextNodes      = [];

        let highlightedNodes         = [];
        highlightedNodes[node.index] = true;

        traverse.forEach( step => {
            node[step.linkType].forEach( link => {
                const n = link[step.nodeType];
                remainingNodes.push( n );
                highlightedNodes[n.index] = true;
            } );

            while ( remainingNodes.length ) {
                nextNodes = [];
                remainingNodes.forEach( node => {
                    node[step.linkType].forEach( link => {
                        const n = link[step.nodeType];
                        if ( !highlightedNodes[n.index] ) {
                            nextNodes.push( n );
                            highlightedNodes[link[step.nodeType].index] = true;
                        }
                    } );
                } );
                remainingNodes = nextNodes;
            }
        } );

        this.node
            .classed( 'highlighted', o => highlightedNodes[o.index] )
            .classed( 'faded', o => !highlightedNodes[o.index] );
        this.link
            .classed( 'highlighted', o => highlightedNodes[o.source.index] && highlightedNodes[o.target.index] )
            .classed( 'faded', o => !highlightedNodes[o.source.index] || !highlightedNodes[o.target.index] );
    }

    // Unhighlight node connections
    restoreConnectedNodes() {
        this.node.classed( "highlighted faded", false );
        this.link.classed( "highlighted faded", false );
    }
}